class CreateWishMaps < ActiveRecord::Migration
  def change
    create_table :wish_maps do |t|
      t.string :wish
      t.string :been
      t.string :ticket
      t.integer :distance
      t.string :uuid

      t.timestamps
    end
  end
end
