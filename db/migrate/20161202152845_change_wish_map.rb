class ChangeWishMap < ActiveRecord::Migration
  def change
    change_column :wish_maps, :been, :text
    change_column :wish_maps, :wish, :text
  end
end
