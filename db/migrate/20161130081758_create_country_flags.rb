class CreateCountryFlags < ActiveRecord::Migration
  def change
    create_table :country_flags do |t|
      t.string :code
      t.string :name
      t.string :population
      t.string :continentName
      t.string :continent
      t.string :isoAlpha3
      t.string :c_name
      t.string :url
      t.string :coordinate

      t.timestamps
    end

    list = JSON.parse open("#{Rails.root}/lib/country_flag.json").read
    list.each do |data|
      CountryFlag.create data
    end

    coordinate_list = JSON.parse open("#{Rails.root}/lib/coordinate.json").read
    coordinate_list.each do |key, value|
      CountryFlag.where(name: key).update_all(coordinate: value)
    end
  end
end
