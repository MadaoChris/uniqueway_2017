require 'rest-client'
class Wechat
  mattr_accessor :app_id
  mattr_accessor :secret_key
  mattr_accessor :store

  attr_accessor :noncestr
  attr_accessor :timestamp

  def self.signature(*args)
    new.signature(*args)
  end

  def self.configure
    yield self if block_given?
  end

  def initialize
    @@app_id = 'wxfcd2a33ea2e94ef4'
    @@secret_key = '032912694b920e7380d7d04f14325a7d'
    @noncestr = SecureRandom.hex
    @timestamp = Time.zone.now.to_i
  end

  def access_token
    access_token = Redis.current.get('wechat:access_token')
    if access_token.blank?
      begin
        response = RestClient.get(
          'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential' \
          "&appid=#{@@app_id}&secret=#{@@secret_key}"
        )
        json = JSON.parse(response.body)

        fail response.body unless json['errcode'].to_i == 0

        access_token = json['access_token']
        Redis.current.set('wechat:access_token', access_token)
        Redis.current.expire('wechat:access_token', 7000)
      rescue => e
        Rails.logger.error e.message
      end
    end

    access_token
  end

  def access_token!
    Redis.current.set('wechat:access_token', nil)
    access_token
  end

  def ticket
    ticket = Redis.current.get('wechat:ticket')
    if ticket.blank?
      begin
        response = RestClient.get(
          'https://api.weixin.qq.com/cgi-bin/ticket/getticket' \
          "?access_token=#{access_token}&type=jsapi"
        )
        json = JSON.parse(response.body)

        fail response.body unless json['errcode'].to_i == 0

        ticket = json['ticket']
        Redis.current.set('wechat:ticket', ticket)
        Redis.current.expire('wechat:ticket', 7000)
      rescue => e
        Rails.logger.error e.message
      end
    end

    ticket
  end

  def ticket!
    Redis.current.set('wechat:ticket', nil)
    ticket
  end

  def signature(url)
    Digest::SHA1.hexdigest "jsapi_ticket=#{ticket}&noncestr=#{noncestr}&timestamp=#{timestamp}&url=#{url}"
  end
end
