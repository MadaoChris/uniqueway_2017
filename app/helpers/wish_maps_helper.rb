module WishMapsHelper
  def map_image(path)
    host = 'http://uniqueway.b0.upaiyun.com/2017_wishmap/'
    image_tag "#{host}#{path}"
  end

  def title_bar(text)
    raw "
    <h2 class='title_bar'>
      #{map_image 'title_bar_left.png'}
      <span>#{text}</span>
      #{map_image 'title_bar_right.png'}
    </h2>
    "
  end

  def share_desc(bool)
    if bool
      # """
      # 未来还有#{@wish_map.distance * 2}里路等着你,你只需决定出发。2017年开始，做个有『鸡』情的人，让别人『鸡』妒到底！
      # """
      countries = CountryFlag.where isoAlpha3: @wish_map.wish
      if countries.count > 1
        if countries.count > 3
          """ 2017年，我要去这些地方：#{countries[0..2].collect(&:c_name).join(',')}...等五个国家及地区，立贴为证！
          """
          
        else
          """ 2017年，我要去这些地方：#{countries.collect(&:c_name).join(',')}，立贴为证！
          """
          
        end
      else
        """
        2017年，我要去#{countries.first.c_name}，立贴为证！
        """
      end
    else      
      """ 2016年，我游历了#{@wish_map.been.count}个国家及地区，已经环游了#{number_to_percentage @wish_map.round_trip_percent, precision: 0}的世界！击败了 #{number_to_percentage @wish_map.wining_percent, precision: 0}的人。
      """
    end.strip
  end
end
