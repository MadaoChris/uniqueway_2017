class CountryFlag < ActiveRecord::Base

  class << self
    def all_flag
      if Redis.current.get('CountryFlag::all_flag')
        JSON.parse(Redis.current.get('CountryFlag::all_flag'))
      else
        list = all.to_a.map(&:attributes)
        Redis.current.set('CountryFlag::all_flag', list.to_json)
        list
      end
    end
  end
end
