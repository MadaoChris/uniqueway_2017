class WishMap < ActiveRecord::Base
  serialize :wish, Array
  serialize :been, Array

  after_create :update_distance

  def countries
    query_list = wish.any? ? wish : been
    CountryFlag.where isoAlpha3: query_list
  end

  def update_distance
    coordinates = [39.938086, 116.426065]
    distance = countries.map do |country|
      distance = Geocoder::Calculations.distance_between(coordinates, country.coordinate.split(',')).to_f
      distance = 0 if distance.nan?
      distance.round(2)
    end.sum

    self.update distance: distance
  end

  def round_trip_percent
    (distance / 40076.0).round(4) * 100
  end

  def wining_percent
    molecules = self.class.where('distance > :distance', distance: distance).count.to_f
    all_count = self.class.count.to_f
    (1 - (molecules/all_count)) * 100
  end
end
