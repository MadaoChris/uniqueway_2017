# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$('body#wishmap').ready ->
  FastClick.attach(document.body)
  guid = ()->
    s4 = ()->
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
  $('#uuid').val(guid)
  UNIQUEWAY_GAME.WISH_MAP = {
    init : ->
      that = @
      that.desc_show(0)
      that.result_show(0)
      @event_bind()

    result_show : (index)->
      that = @
      item = $(".result p:eq(#{index})")
      item.animate({'opacity': 1}, 1000)
      index++
      if $(".result p").length <= index
        return
      else
        setTimeout ->
          that.result_show(index)
        ,1000
      
    desc_show : (index)->
      $('.desc .hide').fadeIn()
      # that = @
      # item = $(".desc .hide:eq(#{index})")
      # item.fadeIn()
      # index++
      # if $(".desc .hide").length <= index
      #   return
      # else
      #   setTimeout ->
      #     that.desc_show(index)
      #   ,300
      
    event_bind : ->
      that = @
      $('.submit_button').on 'click',->
        $('form').submit()

      $('.share_button.go').on 'click',->
        $('.share_block').show()
      $('.share_block').on 'click',->
        $(@).hide()
      
      $('.continent h5').on 'click',->
        $(@).next().toggleClass('hide')
        $(@).find('span').toggleClass('arrow-up arrow-down')
      
      $('input[type="checkbox"]').on "change", ->
        data = {}
        submit_button = $('.submit_button')
        if $(@).is(':checked')
          data[this.value] = {fillKey:"on"}
        else
          data[this.value] = {fillKey:"off"}
        if $('input[type="checkbox"]:checked').length
          submit_button.fadeIn()
        else
          submit_button.hide()
        window.map.updateChoropleth(data, {reset: true})

      $('.button a').on "click",->
        $('.introduce_page').fadeOut(400)
        $('.page-container').fadeIn(400)
        type = $(@).attr('data-type')
        is_wish = type == 'wish'
        $('input[type="checkbox"]').attr('name', "#{type}[]")
        $('.submit_button').text(if is_wish then '生成我的心愿地图' else '生成我的足迹地图')
        $('.flags .title_bar span').text( if is_wish then '2017心愿地图' else '2016年 我已经走过这些地方')
        that.set_up_map()
        if type == 'been'
          $('.wish_bg').remove()
      
      $(".wish_bg").swipe
        swipe: (event, direction, distance, duration, fingerCount, fingerData)->
          $('.wish_bg').animate({
            top: -$(window).height()
          },500)

      $('.wish_bg').on 'click',->
        $('.wish_bg').animate({
          top: -$(window).height()
        },500)
      

    set_up_map : () ->
      if $('#map_container div').length
        return
      data = data || {}
      window.map = new Datamap
        element: document.getElementById("map_container"),
        projection: 'mercator',
        fills  : {
          defaultFill: 'rgb(233, 242, 247)',
          off: 'rgb(233, 242, 247)',
          on: 'rgb(90, 157, 202)'
        },
        geographyConfig: {
          dataUrl: null,
          hideAntarctica: false,
          borderWidth: 1,
          borderColor: 'rgb(90, 157, 202)',
          popupTemplate: (geography, data) ->
            return '<div class="hoverinfo"><strong>' + geography.properties.name + '</strong></div>';
          ,
          popupOnHover: false,
          highlightOnHover: true,
          highlightFillColor: '#958c86',
          highlightBorderColor: '#b5aba3',
          highlightBorderWidth: 0
        },
        data: {},
        bubbleConfig:
          borderWidth: 1,
          borderColor: 'rgba(0, 0, 0, 0.0)',
          popupOnHover: false,
          popupTemplate: (geography, data)->
            return '<div class="hoverinfo"><strong>' + data.name + '</strong></div>';
          ,
        fillOpacity: 0,
        highlightOnHover: true,
        highlightFillColor: 'rgba(0, 0, 0, 0.0)',
        highlightBorderColor: 'rgba(0, 0, 0, 0.0)',
        highlightBorderWidth: 0,
        highlightFillOpacity: 0
      selection_list_height = $(window).height() - 316
      $('.selection_list').height(selection_list_height)
  }

  UNIQUEWAY_GAME.WISH_MAP.init()