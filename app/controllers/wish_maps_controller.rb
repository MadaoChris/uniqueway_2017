class WishMapsController < ApplicationController
  layout 'wishmap'
  before_action :set_wechat

  def index
  end

  def create
    @wish_map = WishMap.create wish_map_params
    Redis.current.set @wish_map.uuid, '1'
    redirect_to wish_map_path(id: @wish_map.uuid)
  end

  def show
    @wish_map = WishMap.find_by uuid: params[:id]
    @title = if @wish_map.been.any?
               "2016足迹地图"
             else
               "2017心愿地图"
             end
  end

  private

  def wish_map_params
    params.permit({ been: [] },{ wish: [] }, :uuid)
  end

  def set_wechat
    @wechat = Wechat.new
  end
end
